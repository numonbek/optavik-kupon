const coupon = document.querySelector(".coupon");
const getCoupon = document.querySelector(".coupon__button");
const myModal = document.querySelector("#myModal");
const closeModal = document.querySelector(".close");
const submitSuccess = document.querySelector("#submitSuccess");
const formModal = document.querySelector("#formModal");
const successModal = document.querySelector(".send-success");
const inputValue = document.querySelectorAll(".input__control");
const ggg = document.querySelector(".telephone");
const errorText = document.querySelectorAll(".input-validation__message");

getCoupon.addEventListener("click", () => {
  formModal.style.display = "block";
  myModal.style.display = "block";
});

submitSuccess.addEventListener("click", (el) => {
  inputValue.forEach((item, index) => {
    item.addEventListener("input", (event) => {
      if (inputValue[0].value !== "") {
        errorText[index].style.display = "none";
      } else {
        errorText[index].style.display = "block";
      }

      if (inputValue[1].value.length <= 15) {
        errorText[1].style.display = "block";
      } else {
        errorText[1].style.display = "none";
      }

      if (inputValue[2].checked == true) {
        errorText[2].style.display = "none";
      } else {
        errorText[2].style.display = "block";
      }
    });
    item.value !== ""
      ? (errorText[index].style.display = "none")
      : (errorText[index].style.display = "block");
    if (inputValue[2].checked == true) {
      errorText[2].style.display = "none";
    } else {
      errorText[2].style.display = "block";
    }
  });

  if (
    inputValue[0].value !== "" &&
    inputValue[1].value.length >= 16 &&
    inputValue[2].checked == true
  ) {
    el.preventDefault();
    formModal.style.display = "none";
    successModal.style.display = "block";
  } else {
    el.preventDefault();
  }
});

closeModal.addEventListener("click", () => {
  myModal.style.display = "none";
  successModal.style.display = "none";
});
